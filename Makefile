all:
	echo "hsc3-db"

clean:
	rm -Rf dist dist-newstyle *~
	(cd cmd ; make clean)

mk-cmd:
	(cd cmd ; make all install)

mk-db-loc:
	sclang scd/gen-db.scd - loc > Sound/SC3/UGen/DB/Data.hs

mk-db-ext:
	sclang scd/gen-db.scd - ext > Sound/SC3/UGen/DB/External/Data.hs

mk-cat:
	sclang scd/gen-cat.scd

push-all:
	r.gitlab-push.sh hsc3-db
