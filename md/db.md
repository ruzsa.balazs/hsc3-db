# hsc3-db

## dmenu

Run [dmenu](https://tools.suckless.org/dmenu/) to select from the UGen
database.

If the command type is `cat` the menu is in two stages, first a
category (and perhaps sub-category) is selected, then a UGen from
within the category.

If the type is `ugen` there is only a single menu, which operates
similarly to completion in an editor.

Each type can be used for the `core` UGens distributed with
SuperCollider, or for `external` UGens.

    $ hsc3-db dmenu cat core

## list

Print a list, separated by spaces, of UGens given category and the language the list is for.

`hs` will write `sinOsc` in place of `SinOsc` &etc.

## xmenu

Run [xmenu](https://github.com/phillbush/xmenu/) to select from the
UGen database hierarchy. The menu can contain either the `core` or
`external` UGens.

    $ hsc3-db xmenu core

![](sw/hsc3-db/md/png/xmenu-cat-core.png)

## scsyndef-to-fs (October, 2014)

A _disassembler_ for UGen graphs, it reads the binary representation
of a [SuperCollider](http://audiosynth.com) _instrument_ and prints an
[hsc3-forth](?t=hsc3-forth) notation of the unit-generator graph.

In the case of simple and direct graphs this notation is rather good, consider:

~~~~
let o = lfSaw KR (mce2 8 7.23) 0 * 3 + 80
    f = lfSaw KR 0.4 0 * 24 + o
    s = sinOsc AR (midiCPS f) 0 * 0.04
in out 0 (combN s 0.2 0.2 4)
~~~~

which is printed as:

~~~~
0 0.4 0 LFSaw.kr 24 * 8 0 LFSaw.kr 3 * 80 + + MIDICPS 0 SinOsc.ar
0.04 * 0.2 0.2 4 CombN 0.4 0 LFSaw.kr 24 * 7.23 0 LFSaw.kr 3 * 80
+ + MIDICPS 0 SinOsc.ar 0.04 * 0.2 0.2 4 CombN Out
~~~~

# Rationale

Used to help translate JMcC’s SC2 examples from haskell to FORTH,
see [hsc3-forth](?t=hsc3-forth).

# Caveat

This should, but does not, print the `mce` instruction for primitives
that halt mce expansion, ie. `EnvGen` and so on.
