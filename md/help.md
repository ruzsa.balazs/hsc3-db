# hsc3-help

## sc3-help rtf

Locates the RTF file for a subject,
runs [unrtf](https://www.gnu.org/software/unrtf/) to write a `.scd` file,
which it opens in [emacs](https://www.gnu.org/software/emacs/).
The RTF files must be at `SC3_RTF_HELP_DIR`.

~~~~
$ echo $SC3_RTF_HELP_DIR
/home/rohan/opt/src/sc3-help/Help
$ hsc3-help sc3-help rtf SinOsc lfSaw softclip
$
~~~~

## sc3-help scdoc-local

Locates entries for some subjects at the
SC3-DOC HTML system and opens them using `BROWSER` or `x-www-browser`.

~~~~
$ echo $SC3_SCDOC_HTML_HELP_DIR
/home/rohan/.local/share/SuperCollider/Help
$ hsc3-help sc3-help scdoc-local SinOsc Collection.inject 'Collection.*fill'
$
~~~~

## ugen exemplar hs

Prints a UGen exemplar for Haskell.

~~~~
$ hsc3-help ugen exemplar hs sinOsc # oscillator
sinOsc ar 440 0
$ hsc3-help ugen exemplar hs resonz # filter
resonz 0 440 1
$ hsc3-help ugen exemplar hs pitch # fixed rate
pitch 0 440 60 4000 100 16 1 0.01 0.5 1 0
$
~~~~

## ugen exemplar hs/control

Prints a UGen exemplar for Haskell with named control inputs.

~~~~
$ hsc3-help ugen exemplar hs/control pitch
pitch kr (control kr "in" 0.0) (control kr "initFreq" 440.0) ...
$
~~~~

## ugen exemplar hs/control/let

Prints a UGen exemplar for Haskell with named control values bound to local variables.

~~~~
$ hsc3-help ugen exemplar hs/control/let gverb
let roomsize = control kr "roomsize" 10.0
    revtime = control kr "revtime" 3.0
    damping = control kr "damping" 0.5
    inputbw = control kr "inputbw" 0.5
    spread = control kr "spread" 15.0
    drylevel = control kr "drylevel" 1.0
    earlyreflevel = control kr "earlyreflevel" 0.7
    taillevel = control kr "taillevel" 0.5
    maxroomsize = control kr "maxroomsize" 300.0
in gVerb ... roomsize revtime damping inputbw spread drylevel earlyreflevel ...
$
~~~~

## ugen exemplar hs/record

Prints a UGen exemplar in `hsc3-rec` record form.

~~~~
$ hsc3-help ugen exemplar hs/record sinosc lfsaw dseq pv_add in
SinOsc {rate=ar, freq=440, phase=0}
LFSaw {rate=ar, freq=440, iphase=0}
Dseq {rate=dr, uid='α', repeats=1, list=0}
PV_Add {rate=kr, bufferA=0, bufferB=0}
In {nc=1, rate=ar, bus=0}
$
~~~~

## ugen exemplar sc/keyword

Prints a UGen exemplar in SuperCollider keyword form.

~~~~
$ hsc3-help ugen exemplar sc/keyword sinosc lfsaw dseq pv_add in
SinOsc.ar(freq: 440.0, phase: 0.0)
LFSaw.ar(freq: 440.0, iphase: 0.0)
Dseq.new(repeats: 1.0, list: 0.0)
PV_Add.new(bufferA: 0.0, bufferB: 0.0)
In.ar(numChannels: 1, bus: 0.0)
$
~~~~

## ugen exemplar st

Prints a UGen exemplar for Smalltalk.

~~~~
$ hsc3-help ugen exemplar st sinosc lfsaw dseq pv_add in
SinOsc freq: 440.0 phase: 0.0
LFSaw freq: 440.0 iphase: 0.0
Dseq repeats: 1.0 list: 0.0
PV_Add bufferA: 0.0 bufferB: 0.0
In numChan: 1 bus: 0.0
$
~~~~

## ugen summary

Prints UGen information from [hsc3-db](?t=hsc3-db).

~~~~
$ hsc3-help ugen summary bufRd demand
Buffer reading oscillator.

BufRd kr|ar bufnum=0.0 phase=0.0 loop=1.0 interpolation=2.0
    NC INPUT: True, ENUMERATION INPUTS: 2=Loop, 3=Interpolation

Demand results from demand rate UGens.

Demand kr|ar trig=0.0 reset=0.0 *demandUGens=0.0
    MCE=1, FILTER: TRUE
~~~~
