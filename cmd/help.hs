import System.Environment {- base -}

import qualified Sound.Sc3.Common.Help as Help {- hsc3 -}

import qualified Sound.Sc3.Ugen.Db as Db {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Pp as Db {- hsc3-db -}

{-

ugen-default-param-named =>
 let {gate=1.0, levelScale=1.0, levelBias=0.0, timeScale=1.0, doneAction=0.0, *envelope=0.0}
 in envGen AR gate levelScale levelBias timeScale doneAction envelope

-}

exemplar_hs_control :: String -> String
exemplar_hs_control nm = maybe ("error: no entry: " ++ nm) (Db.u_control_inputs_pp True) (Db.u_lookup_ci nm)

exemplar_hs_control_let :: String -> String
exemplar_hs_control_let nm = maybe ("error: no entry: " ++ nm) Db.u_control_param_pp (Db.u_lookup_ci nm)

exemplar_hs :: String -> String
exemplar_hs nm = maybe ("error: no entry: " ++ nm) (Db.u_default_param True) (Db.u_lookup_ci nm)

exemplar_smalltalk :: String -> String
exemplar_smalltalk nm = maybe ("error: no entry: " ++ nm) Db.u_smalltalk_pp (Db.u_lookup_ci nm)

exemplar_sclanguage :: Bool -> String -> String
exemplar_sclanguage with_kw nm = maybe ("error: no entry: " ++ nm) (Db.u_sclanguage_pp with_kw) (Db.u_lookup_ci nm)

exemplar_hs_record :: String -> String
exemplar_hs_record nm = maybe ("error: no entry: " ++ nm) Db.u_hsc3_rec_pp (Db.u_lookup_ci nm)

help :: [String]
help =
    ["hsc3-help command [arguments]"
    ," sc3-help {rtf|scdoc{local|online}} subject..."
    ," ugen exemplar {hs{/control{/let}|/record} | sc{/keyword} | st} ugen-name..."
    ," ugen summary ugen-name..."]

{-
> run = main_with_args . words
> run "ugen exemplar hs sinosc whiteNoise drand pitch"
> run "ugen exemplar hs/control sinosc whiteNoise drand pitch"
> run "ugen exemplar hs/control/let sinosc whiteNoise drand pitch"
> run "ugen exemplar hs/record sinosc whiteNoise drand pitch pv_brickwall in"
> run "ugen exemplar sc sinosc whiteNoise drand pitch pv_brickwall in"
> run "ugen exemplar sc/keyword sinosc whiteNoise drand pitch pv_brickwall in"
> run "ugen exemplar st sinosc whiteNoise drand pitch pv_add in"
> run "ugen summary sinosc whiteNoise drand pitch pv_add in"
-}
main_with_args :: [String] -> IO ()
main_with_args arg =
  case arg of
    "sc3-help":"rtf":x -> mapM_ Help.sc3_rtf_help_scd_open_emacs x
    "sc3-help":"scdoc-local":x -> mapM_ (Help.sc3_scdoc_help_open True . Help.sc3_scdoc_help_path) x
    "sc3-help":"scdoc-online":x -> mapM_ (Help.sc3_scdoc_help_open False . Help.sc3_scdoc_help_path) x
    "ugen":"exemplar":"hs":u -> mapM_ (putStrLn . exemplar_hs) u
    "ugen":"exemplar":"hs/control":u -> mapM_ (putStrLn . exemplar_hs_control) u
    "ugen":"exemplar":"hs/control/let":u -> mapM_ (putStrLn . exemplar_hs_control_let) u
    "ugen":"exemplar":"hs/record":u -> mapM_ (putStrLn . exemplar_hs_record) u
    "ugen":"exemplar":"sc":u -> mapM_ (putStrLn . exemplar_sclanguage False) u
    "ugen":"exemplar":"sc/keyword":u -> mapM_ (putStrLn . exemplar_sclanguage True) u
    "ugen":"exemplar":"st":u -> mapM_ (putStrLn . exemplar_smalltalk) u
    "ugen":"summary":u -> mapM_ Db.ugen_summary_wr u
    _ -> putStrLn (unlines help)

main :: IO ()
main = getArgs >>= main_with_args
