-- | SuperCollider UGen Bindings
module Sound.Sc3.Ugen.Db.Bindings.SuperCollider where

import Data.List {- base -}
import Text.Printf {- base -}

import qualified Sound.Sc3.Common.Rate as Rate {- hsc3 -}

import qualified Sound.Sc3.Ugen.Db.Meta as Meta {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Record as Db {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Rename as Rename {- hsc3-db -}

-- | If U is reordered by Sc then transform filter index (forwards).
ix_reordered :: Db.U -> Int
ix_reordered u =
  case Db.ugen_reorder u of
    Nothing -> Db.u_filter_ix u
    Just x -> Meta.reorder_index_forwards x (Db.u_filter_ix u)

-- | UGens that already have filter methods.
sc_filter_method_ignore_list :: [String]
sc_filter_method_ignore_list =
  ["CheckBadValues","Clip","DegreeToKey","Fold","Lag","Lag2","Lag3","Poll","Sanitize","Select","Slew","UnaryOpUGen","Wrap"]

-- | Filter method for U.
sc_filter_method :: Db.U -> String
sc_filter_method u =
  let delete_at k l = let (p,q) = splitAt k l in p ++ tail q
      nm = Db.ugen_name u
      mth = Rename.fromSc3Name nm
      ix = ix_reordered u
      reo = case Db.ugen_reorder u of
              Nothing -> Db.ugen_inputs u
              Just x -> Meta.apply_reorder_backwards x (Db.ugen_inputs u)
      inp = map Db.input_name reo
      def = map Db.input_default reo
      self = inp !! ix
      jn = (\i j -> concat [i," = ",show j])
      arg = if length inp == 1
            then ""
            else concat ["    arg ", intercalate ", " (zipWith jn (delete_at ix inp) (delete_at ix def)), ";\n"]
      cons = map (\x -> if x == self then "this" else x) inp
  in printf "  %s {\n%s    ^%s.performList(this.rate.rateToSelector, [%s])\n  }" mth arg nm (intercalate ", " cons)

sc_filter_methods :: [Db.U] -> String
sc_filter_methods u =
  unlines
  ["+ UGen {"
  ,unlines (map sc_filter_method u)
  ,"}"
  ,"+ Array {"
  ,unlines (map sc_filter_method u)
  ,"}"]

-- * First input message

-- | UGens to pass over for first input message forms.
sc_first_input_ignore_list :: [String]
sc_first_input_ignore_list = ["In"]

-- | Method for U where the first input is selected as the input parameter.
sc_first_input_method :: String -> Db.U -> String
sc_first_input_method cl u =
  let nm = Db.ugen_name u
      mth = Rename.fromSc3Name nm
      reo = case Db.ugen_reorder u of
              Nothing -> Db.ugen_inputs u
              Just x -> Meta.apply_reorder_backwards x (Db.ugen_inputs u)
      inp = map Db.input_name reo
      def = map Db.input_default reo
      jn = (\i j -> concat [i," = ",show j])
      arg = if length inp == 1
            then ""
            else concat ["arg ", intercalate ", " (zipWith jn (tail inp) (tail def)), "; "]
      rt = concat ["'",Rate.rateName (Db.ugen_default_rate u),"'"]
  in printf "+ %s { %s { %s ^%s.multiNew(%s, %s) } }" cl mth arg nm rt (intercalate ", " ("this" : tail inp))

sc_first_input_methods :: [Db.U] -> String
sc_first_input_methods u =
  unlines
  [unlines (map (sc_first_input_method "UGen") u)
  ,unlines (map (sc_first_input_method "Array") u)
  ,unlines (map (sc_first_input_method "SimpleNumber") u)]

-- * Filter Constructor

-- | Filter UGens that already have *new methods, or that have more complex rules.
sc_filter_constructor_ignore_list :: [String]
sc_filter_constructor_ignore_list = ["MulAdd", "Select"]

-- | Filter constructor with implicit rate for U.
sc_filter_constructor :: Db.U -> String
sc_filter_constructor u =
  let nm = Db.ugen_name u
      reo = case Db.ugen_reorder u of
              Nothing -> Db.ugen_inputs u
              Just x -> Meta.apply_reorder_backwards x (Db.ugen_inputs u)
      inp = map Db.input_name reo
      def = map Db.input_default reo
      jn = (\i j -> concat [i," = ",show j])
      arg = if length inp == 0
            then ""
            else concat ["arg ", intercalate ", " (zipWith jn inp def), ";"]
      rt = concat [inp !! ix_reordered u, ".rate.rateToSelector"]
  in printf "+ %s { *new { %s^%s.performList(%s, [%s]) } }" nm arg nm rt (intercalate ", " inp)

sc_filter_constructors :: [Db.U] -> String
sc_filter_constructors = unlines . map sc_filter_constructor

-- * Implicit Rates

sc_implicit_rate_ignore_list :: [String]
sc_implicit_rate_ignore_list =
  ["ClearBuf"
  ,"Dbufrd","Dbufwr","Diwhite","Drand","Dseq","Dshuf","Dseries","Dwhite"
  ,"ExpRand"
  ,"FFT"
  ,"IFFT","InFeedback", "IRand"
  ,"LinRand","LocalBuf"
  ,"MaxLocalBufs"
  ,"NRand"
  ,"PV_RandComb"
  ,"Rand"
  ,"SetBuf"]

-- | Implicit rate constructor for U.
sc_implicit_rate_constructor :: Db.U -> String
sc_implicit_rate_constructor u =
  let nm = Db.ugen_name u
      nc x = if Db.ugen_nc_input u then Db.I "numChannels" 1 : x else x
      reo = nc (case Db.ugen_reorder u of
                   Nothing -> Db.ugen_inputs u
                   Just x -> Meta.apply_reorder_backwards x (Db.ugen_inputs u))
      inp = map Db.input_name reo
      def = map Db.input_default reo
      jn = (\i j -> concat [i," = ",show j])
      arg = if length inp == 0
            then ""
            else concat ["arg ", intercalate ", " (zipWith jn inp def), "; "]
      rt = concat ["'",Rate.rateName (Db.ugen_default_rate u),"'"]
  in printf "+ %s { *new { %s^%s.performList(%s.rateToSelector, [%s]) } }" nm arg nm rt (intercalate ", " inp)

sc_implicit_rate_constructors :: [Db.U] -> String
sc_implicit_rate_constructors = unlines . map sc_implicit_rate_constructor
