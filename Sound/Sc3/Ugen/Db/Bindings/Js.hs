-- | JavaScript/TypeScript Ugen Bindings
module Sound.Sc3.Ugen.Db.Bindings.Js where

import Data.Char {- base-}
import Data.List {- base -}
import Data.Maybe {- base -}
import Text.Printf {- base -}

import Sound.Sc3.Common.Rate {- hsc3 -}

import qualified Sound.Sc3.Ugen.Db as Db {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Bindings.Core as Bindings {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Record as Db {- hsc3-db -}

{- | Rate here indicates the maximum supported rate (for Oscillators) or the input names (for filters).

> unwords (map (js_rate_id . Left) [minBound .. maxBound]) == "rateIr rateKr rateAr rateDr"
> js_rate_id (Right [0, 1]) == "[0, 1]"
-}
js_rate_id :: Either Rate [Int] -> String
js_rate_id r =
  let capitalise x = toUpper (head x) : tail x
  in case r of
       Left rt -> "rate" ++ capitalise (rateAbbrev rt)
       Right flt -> concat ["[",intercalate ", " (map show flt),"]"]

js_param_nm_rewrite :: String -> String
js_param_nm_rewrite x =
  case x of
    "in" -> "input"
    "default" -> "defaultValue"
    _ -> x

ts_add_annotation :: String -> String -> String
ts_add_annotation annot txt = printf "%s: %s" txt annot

maybe_ts_add_annotation :: String -> Maybe String -> Maybe String
maybe_ts_add_annotation annot = fmap (ts_add_annotation annot)

maybe_maybe_ts_add_annotation :: Bool -> String -> Maybe String -> Maybe String
maybe_maybe_ts_add_annotation typeSig annot = fmap (if typeSig then ts_add_annotation annot else id)

{- | Generate Js Ugen function.

> let rw nm rt dsc nc sp = putStrLn . js_cons True nm rt dsc nc sp
> rw "SinOsc" (Left ar) "..." (Right 1) 0 (["freq","phase"],Nothing)
> rw "Pan2" (Right [0]) "..." (Right 2) 0 (["in","pos","level"],Nothing)
> rw "In" (Left ar) "..." (Left (Left ())) 0 (["bus"],Nothing)
> rw "BrownNoise" (Left ar) "..." (Right 1) 0 ([],Nothing)
> rw "MulAdd" (Right [0]) "..." (Right 1) 0 (["in","mul","add"],Nothing)
-}
js_cons :: Bool -> String -> Either Rate [Int] -> String -> Bindings.Num_Chan -> Int -> Bindings.Param_Nm -> String
js_cons typeSig ugen_nm ugen_rt ugen_dsc num_chan special (param_nm,mce_nm) =
  let mcons e l = maybe l (:l) e
      typ_add inSig = if inSig && typeSig then ts_add_annotation "Signal" else id
      param_nm_rw inSig = map (typ_add inSig . js_param_nm_rewrite) param_nm
      mce_nm_rw inSig = fmap (map (typ_add inSig . js_param_nm_rewrite)) mce_nm
      all_nm = mcons
               (maybe_maybe_ts_add_annotation typeSig "number" (Bindings.num_chan_pseudo_input "numChan" num_chan))
               (Bindings.all_param_ty (param_nm_rw True,mce_nm_rw True))
      mceInput =
        case mce_nm_rw False of
          Just [nm] -> printf "arrayLength(asArray(%s))" nm
          _ -> error "mceInput?"
      nc_nil = fromMaybe "numChan" (Bindings.num_chan_mce_input (const mceInput) num_chan)
      rateText = js_rate_id ugen_rt
      asArray = printf "asArray(%s)"
      inp_nm = case mce_nm_rw False of
                 Nothing -> printf "[%s]" (intercalate ", " (param_nm_rw False))
                 Just mce -> printf "arrayConcat([%s], (%s))" (intercalate ", " (param_nm_rw False)) (intercalate ", " (map asArray mce))
  in unlines
     [printf "// %s" ugen_dsc
     ,printf "export function %s(%s)%s {" ugen_nm (intercalate ", " all_nm) (if typeSig then ": Signal" else "")
     ,printf
       "    return makeUgen('%s', %s, %s, %d, %s);"
       ugen_nm (Bindings.num_chan_fixed_str nc_nil num_chan) rateText special (inp_nm :: String)
     ,"}"]

{- | Generate Smalltalk definitions for U

> pp = putStrLn . js_gen_u True . Db.u_lookup_cs_err
> mapM_ pp ["LFGauss","EnvGen","LinExp","FM7","In"]
> mapM_ pp ["SinOsc", "HPF", "Pan2", "PinkNoise", "Out", "LFSaw", "CombN", "MulAdd"]
-}
js_gen_u :: Bool -> Db.U -> String
js_gen_u typeSig u =
    let ugen_nm = Db.ugen_name u
        ugen_rt = maybe (Left (Db.ugen_default_rate u)) Right (Db.ugen_filter u)
        param_nm = Bindings.u_param_nm u
        num_chan = Bindings.u_num_chan u
        ugen_dsc = Db.ugen_summary u
    in js_cons typeSig ugen_nm ugen_rt ugen_dsc num_chan 0 param_nm

js_gen_binop :: Bool -> (String,Int) -> String
js_gen_binop typeSig (sym,ix) =
  let ty = if typeSig then ": Signal" else ""
  in printf "export function %s(a%s, b%s)%s { return BinaryOp(%d, a, b); }" sym ty ty ty ix

js_gen_uop :: Bool -> (String,Int) -> String
js_gen_uop typeSig (sym,ix) =
  let ty = if typeSig then ": Signal" else ""
  in printf "export function %s(a%s)%s { return UnaryOp(%d, a); }" sym ty ty ix

js_sc3_gen_bindings :: Bool -> [(String, Int)] -> [(String, Int)] -> [String] -> [String]
js_sc3_gen_bindings typeSig uop binop ugen =
  let f = js_gen_u typeSig . Db.u_lookup_cs_err
  in [concatMap f ugen
     ,unlines (map (js_gen_binop typeSig) binop)
     ,unlines (map (js_gen_uop typeSig) uop)]

js_sc3_preamble :: [String]
js_sc3_preamble =
  ["import { asArray, arrayConcat, arrayLength} from './sc3-array.js'"
  ,"import { rateAr, rateDr, rateIr, rateKr } from './sc3-rate.js'"
  ,"import { BinaryOp, Signal, UnaryOp, makeUgen } from './sc3-ugen.js'"]

js_sc3_gen_bindings_wr :: Bool -> FilePath -> [(String, Int)] -> [(String, Int)] -> [String] -> IO ()
js_sc3_gen_bindings_wr typeSig fn uop binop =
  writeFile fn .
  unlines .
  (if typeSig then id else ("'use strict';\n" :)) .
  (js_sc3_preamble ++) .
  js_sc3_gen_bindings typeSig uop binop

js_rewrite_op_name :: String -> String
js_rewrite_op_name x = if False then toLower (head x) : tail x else x

-- > putStrLn $ unlines $ map (js_gen_uop True) js_sc3_uop
js_sc3_uop :: [(String,Int)]
js_sc3_uop = zip (map js_rewrite_op_name Db.uop_names) [0..]

-- > putStrLn $ unlines $ map (js_gen_binop True) js_sc3_binop
js_sc3_binop :: [(String,Int)]
js_sc3_binop = zip (map js_rewrite_op_name Db.binop_names) [0..]

-- > putStrLn (js_sc3_op_object "unaryOperators" js_sc3_uop)
-- > putStrLn (js_sc3_op_object "binaryOperators" js_sc3_binop)
js_sc3_op_object :: String -> [(String,Int)] -> String
js_sc3_op_object ty op =
  let f (nm, ix) = printf "  %s: %d," nm ix
      e = map f op
      sig = ": OperatorDictionary"
  in unlines (printf "var %s%s = {" ty sig : e ++ ["}"])
