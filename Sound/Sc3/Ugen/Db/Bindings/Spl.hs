module Sound.Sc3.Ugen.Db.Bindings.Spl where

import Data.Char {- base-}
import Data.List {- base -}
import Text.Printf {- base -}

import Sound.Sc3.Common.Math.Operator {- hsc3 -}

import qualified Sound.Sc3.Ugen.Db as Db {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Rename as Rename {- hsc3-db -}
import Sound.Sc3.Ugen.Db.Record {- hsc3-db -}

{- | Generate Spl entry to bind to Js procedure.

> pp nm = let Just u = Db.u_lookup_cs nm in spl_entry "sc." u
> pp "SinOsc" == "SinOsc { :freq :phase | <primitive: return sc.SinOsc(_freq, _phase);> }"
> pp "WhiteNoise" == "WhiteNoise { <primitive: return sc.WhiteNoise();> }"
> pp "LocalIn" == "LocalIn { :numChan :default | <primitive: return sc.LocalIn(_numChan, _default);> }"
-}
spl_entry :: String -> U -> String
spl_entry prefix u =
  let nc = if ugen_nc_input u then [I "numChan" 1] else []
      pr = map input_name (nc ++ ugen_inputs u)
      nm = Rename.sc3_ugen_rename (ugen_name u)
      prL = if null pr then "" else (unwords (map (':' :) pr) ++ " | ")
      prR = intercalate ", " (map ("_" ++ ) pr)
  in printf "%s { %s<primitive: return %s%s(%s);> }" nm prL prefix nm prR

{- | Generate Spl keyword (dictionary) entry.

> pp nm = let Just u = Db.u_lookup_cs nm in spl_dict_entry u
> pp "SinOsc" == "SinOsc { :d | SinOsc(d::freq ? 440.0, d::phase ? 0.0) }"
> pp "WhiteNoise" == ""
> pp "GrainFM"
-}
spl_dict_entry :: U -> String
spl_dict_entry u =
  if length (ugen_inputs u) == 0
  then ""
  else let nc = if ugen_nc_input u then [I "numChan" 1] else []
           pr = map (\i -> printf "d::%s ? %f" (input_name i) (input_default i)) (nc ++ ugen_inputs u)
           nm = ugen_name u
       in printf "%s { :d | %s(%s) }" nm nm (intercalate ", " pr)

spl_void_block :: String -> [U] -> String
spl_void_block prefix db =
  let db' = filter ((== 0) . u_num_inputs) db
      e = map (('\t' :) . spl_entry prefix) db'
  in unlines (concat [["+ Void {"], e, ["}"]])

spl_object_block :: String -> [U] -> String
spl_object_block prefix db =
  let db' = filter ((> 0) . u_num_inputs) db
      e = map (('\t' :) . spl_entry prefix) db'
  in unlines (concat [["+ Object {"], e, ["}"]])

spl_dict_block :: [U] -> String
spl_dict_block db =
  let db' = filter ((> 0) . u_num_inputs) db
      e = map (('\t' :) . spl_dict_entry) db'
  in unlines (concat [["+ IdentityDictionary {"], e, ["}"]])

spl_lower :: String -> String
spl_lower x = if False then toLower (head x) : tail x else x

spl_gen_uop :: String -> (String, String) -> String
spl_gen_uop prefix (sc, spl) =
  printf "\t%s { :self | <primitive: return %s%s(_self);> }" spl prefix (spl_lower sc)

spl_gen_binop :: String -> (String, String) -> String
spl_gen_binop prefix (sc, spl) =
  printf "\t%s { :self :anObject | <primitive: return %s%s(_self, _anObject);> }" spl prefix (spl_lower sc)

{- | Uop selection

> (length uop_list, fromEnum (maxBound :: Sc3_Unary_Op))
-}
uop_list :: [Sc3_Unary_Op]
uop_list = map toEnum [0,5,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,25,26,27,28,29,30,36,42,43]

{- | Binop selection

> (length binop_list, fromEnum (maxBound :: Sc3_Binary_Op))
-}
binop_list :: [Sc3_Binary_Op]
binop_list = map toEnum [0,1,2,3,4,5,7,8,9,10,11,12,13,14,15,17,18,19,21,23,25,26,27,40,41,42,44]

{- | Ugen selection (core)

> Data.List.sort ugen_list_core == ugen_list_core
> filter (not . Db.ugen_is_core) ugen_list_core == []
-}
ugen_list_core :: [String]
ugen_list_core =
  ["AllpassC","AllpassL","AllpassN","AmpComp","AmpCompA","Amplitude"
  ,"BBandPass","BBandStop","BHiPass","BLowPass","BPF","BPZ2","BPeakEQ","BRF","BRZ2"
  ,"Balance2","Blip","BlockSize","BrownNoise","BufDur","BufFrames","BufRateScale","BufRd","BufSampleRate","BufWr"
  ,"ClearBuf","Clip","ClipNoise","CoinGate","CombC","CombL","CombN","Compander","ControlDur","ControlRate","Convolution","Crackle","CuspL","CuspN"
  ,"DC","Dbufrd","Dbufwr","Decay","Decay2","DegreeToKey","Delay1","Delay2","DelayC","DelayL","DelayN","Demand","DetectSilence","Diwhite","Drand","Dseq","Dseries","Dshuf","Dswitch1", "Dswitch", "Dust","Dust2","Duty","Dwhite","Dxrand"
  ,"EnvGen","ExpRand"
  ,"FBSineC","FBSineL","FFT","FOS","FSinOsc","Fold","Formant","Formlet","FreeVerb","FreeVerb2","FreqShift"
  ,"GVerb","Gate","Gendy1","GrainBuf","GrainFM","GrainSin","GrayNoise"
  ,"HPF","HPZ1","HPZ2","Hasher","HenonC","HenonL","HenonN"
  ,"IFFT","IRand","Impulse","In","InFeedback","InRange","Index","IndexInBetween","Integrator"
  ,"K2A","KeyState","Klang","Klank"
  ,"LFClipNoise","LFCub","LFDNoise1","LFDNoise3","LFGauss","LFNoise0","LFNoise1","LFNoise2","LFPar","LFPulse","LFSaw","LFTri","LPF","LPZ1","LPZ2"
  ,"Lag","Lag2","Lag3","Lag3UD","LagUD","Latch","LatoocarfianC","LeakDC","Limiter","LinCongC","LinExp","LinPan2","LinRand","LinXFade2","Line","Linen","LocalBuf","LocalIn","LocalOut","Logistic","LorenzL"
  ,"MantissaMask","MaxLocalBufs","Median","MidEQ","ModDif","MoogFF","MouseButton","MouseX","MouseY","MulAdd"
  ,"NRand","Normalizer","NumOutputBuses"
  ,"OnePole","OneZero","Osc","Out"
  ,"PV_Diffuser","PV_RandComb"
  ,"Pan2","PanAz","PanB","PeakFollower","Phasor","PinkNoise","Pitch","PitchShift","PlayBuf","Pluck","Pulse","PulseCount","PulseDivider"
  ,"QuadC","QuadL"
  ,"RHPF","RLPF","Rand","RecordBuf","ReplaceOut","Resonz","Ringz","Rotate2","RunningMax","RunningSum"
  ,"SOS","SampleDur","SampleRate","Sanitize","Saw","Schmidt","Select","SetBuf","SetResetFF","SinOsc","SinOscFB","Slew","Slope","Spring","StandardL","StandardN","Stepper","Sweep","SyncSaw"
  ,"TDelay","TDuty","TExpRand","TGrains","TIRand","TRand","Timer","ToggleFF","Trig","Trig1","TwoPole","TwoZero"
  ,"VarSaw","Vibrato"
  ,"Warp1","WhiteNoise","Wrap","WrapIndex"
  ,"XFade2","XLine"
  ,"ZeroCrossing"]

-- | Ugen selection (ext)
ugen_list_ext :: [String]
ugen_list_ext =
  ["DFM1" -- sc3-plugins/TJUGens
  ,"DWGPluckedStiff" -- sc3-plugins/DWGUGens
  ,"SinGrain" -- sc3-plugins/JoshUGens
  ,"LFBrownNoise1", "MoogLadder" -- sc3-plugins/BhobUGens
  ,"GreyholeRaw" -- sc3-plugins/DEIND
  ,"CrossoverDistortion" -- sc3-plugins/Distortion
  ,"Friction", "Perlin3", "WaveLoss" -- sc3-plugins/MCLD
  ,"MembraneCircle" -- sc3-plugins/Membrane
  ,"VOSIM" -- sc3-plugins/VOSIM
  ,"MiBraids", "MiClouds", "MiRings" -- mi-UGens
  ,"AnalogFoldOsc" -- portedplugins
  ,"RCD","SCM","VBJonVerb" -- v7b1/vb_UGens
  ,"DustRange","ExpRandN","LinRandN","RandN" -- sc3-rdu
  ,"TLinRand","TScramble" -- sc3-rdu
  ,"Dx7","Dx7Env","ObxdFilter","SvfBp","SvfHp","SvfLp" -- sc3-rdu
  ,"Bezier","Freezer" -- sc3-rdu ,"ShufflerB"
  ]

is_core :: U -> Bool
is_core u = ugen_name u `elem` ugen_list_core

is_ext :: U -> Bool
is_ext u = ugen_name u `elem` ugen_list_ext

u_gen_spl :: IO ()
u_gen_spl = do
  let cpy x = (x,x)
  putStrLn $ spl_void_block "sc." (filter is_core Db.ugen_db_core)
  putStrLn $ spl_void_block "sc." (filter is_ext Db.ugen_db_ext)
  putStrLn $ spl_object_block "sc." (filter is_core Db.ugen_db_core)
  putStrLn $ spl_object_block "sc." (filter is_ext Db.ugen_db_ext)
  putStrLn $ spl_dict_block Db.ugen_db_core
  putStrLn $ unlines $ map (spl_gen_uop "sc." . cpy) (map sc3_unary_op_name uop_list)
  putStrLn $ unlines $ map (spl_gen_binop "sc." . cpy) (map sc3_binary_op_name binop_list)
