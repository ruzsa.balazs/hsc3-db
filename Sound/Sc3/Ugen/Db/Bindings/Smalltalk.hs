-- | Smalltalk UGen Bindings
module Sound.Sc3.Ugen.Db.Bindings.Smalltalk where

import Data.Char {- base -}
import Data.List {- base -}
import Data.Maybe {- base -}
import Text.Printf {- base -}

import Sound.Sc3.Common.Rate {- hsc3 -}

import qualified Sound.Sc3.Ugen.Db as Db {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Bindings.Core as Bindings {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Pseudo as Pseudo {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Record as Db {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Rename as Rename {- hsc3-db -}

-- * Pp

st_class_pp :: (String, String) -> [String] -> [String] -> String -> String -> String
st_class_pp (super_class_name, class_name) instance_var class_var category comment =
  unlines [printf "%s subclass: #%s" super_class_name class_name
          ,printf "  instanceVariableNames: '%s'" (unwords instance_var)
          ,printf "  classVariableNames: '%s'" (unwords class_var)
          ,"  poolDictionaries: ''"
          ,printf "  category: '%s'!" category
          ,""
          ,printf "%s comment: '%s'!" class_name comment]

-- * Bindings

{- | Rate here indicates the maximum supported rate (for Oscillators) or the input indices (for filters).
Smalltalk is one-indexed.

> unwords (map (st_rate_id . Left) [minBound .. maxBound]) == "(Rate ir) (Rate kr) (Rate ar) (Rate dr)"
> st_rate_id (Right [0, 1]) == "#(1 2)"
-}
st_rate_id :: Either Rate [Int] -> String
st_rate_id r =
  case r of
    Left rt -> printf "(Rate %s)" (map toLower (rateAbbrev rt))
    Right flt -> printf "#(%s)" (unwords (map (show . succ) flt))

{- | Generate Smalltalk class definition.

> putStrLn $ st_class "SinOsc" "Sine oscillator." (["freq","phase"],Nothing)
> putStrLn $ st_class "Out" "Write a signal to a bus." (["bus"],Just ["channelsArray"])
-}
st_class :: String -> String -> Bindings.Param_Nm -> String
st_class ugen_nm ugen_dsc _param_nm = st_class_pp ("ScUgen", Rename.sc3_ugen_rename ugen_nm) [] [] "Sound-Sc3" ugen_dsc

{- | Add ! and run unlines.

> unlines_with_point ["a","b"] == "a\nb\n!\n"
-}
unlines_with_point :: [String] -> String
unlines_with_point = unlines . flip (++) ["!"]

param_all_nm :: Bindings.Num_Chan -> Bindings.Param_Nm -> [String]
param_all_nm num_chan (param_nm,mce_nm) =
  let mcons e l = maybe l (:l) e
  in mcons
     (Bindings.num_chan_pseudo_input "numChan" num_chan)
     (Bindings.all_param_ty (param_nm,mce_nm))

u_all_nm :: Db.U -> [String]
u_all_nm u = param_all_nm (Bindings.u_num_chan u) (Bindings.u_param_nm u)

{- | A table mapping Ugen names to primaryFactoryMethod selector.

> map (fromJust . flip lookup scApplySelectorTable) ["In", "Saw", "SinOsc", "Out", "Seq", "DmdOn"]
-}
scApplySelectorTable :: [(String, String)]
scApplySelectorTable =
  let pp = concatMap (\i -> i ++ ":")
      f u = (Db.ugen_name u
            ,pp (u_all_nm u))
      g (nm, inp, _, _, _) = (nm, pp inp)
  in map f Db.ugen_db ++ map g Pseudo.pseudo_ugen_db

-- | Smalltalk instance creation method
type St_Icm = (String, Either Rate [Int], Bindings.Num_Chan, Int, Bindings.Param_Nm)

{- | Generate Smalltalk instance creation methods (rate, keyword inputs, with mul: and add:).
     MulAdd is a special case, it does not have additional mul and add inputs, and it has an optimization pass.
-}
st_instance_creation_methods :: St_Icm -> [(String, String)]
st_instance_creation_methods (ugen_nm, ugen_rt, num_chan, special, (param_nm,mce_nm)) =
  let mcons e l = maybe l (:l) e
      all_nm = mcons
               (Bindings.num_chan_pseudo_input "numChan" num_chan)
               (Bindings.all_param_ty (param_nm,mce_nm))
      pat ext = case all_nm ++ ext of
                  [] -> "new"
                  param -> unwords (map (\x -> printf "%s: %s" x x) param)
      consText = if null all_nm
                 then printf "%s new" (Rename.sc3_ugen_rename ugen_nm)
                 else unwords (Rename.sc3_ugen_rename ugen_nm : concatMap (\x -> [x ++ ":",x]) all_nm)
      mceInput =
        case mce_nm of
          Just [nm] -> printf "%s asArray size" nm
          _ -> error "mceInput?"
      nc_nil = fromMaybe "numChan" (Bindings.num_chan_mce_input (const mceInput) num_chan)
      rateText = st_rate_id ugen_rt
      asArray = printf "%s asArray"
      jn_dot = intercalate ". "
      inp_nm :: String
      inp_nm = case mce_nm of
                 Nothing -> printf "{%s}" (jn_dot param_nm)
                 Just mce -> printf "({%s} , (%s))" (jn_dot param_nm) (jn_dot (map asArray mce))
      pfm =
        ("primaryFactoryMethod"
        ,printf "^#%s" (if null all_nm then "new" else concat (map (\x -> x ++ ":") all_nm)))
  in (filter (not . null) . concat)
     [[(pat []
       ,printf
         "^UgenBuilder name: '%s' numChan: %s rateSpec: %s specialIndex: %d inputArray: %s"
         ugen_nm (Bindings.num_chan_fixed_str nc_nil num_chan) rateText special inp_nm)
      ,pfm]
     ,if ugen_nm == "MulAdd"
      then []
      else [(pat ["mul"], printf "^(%s) * mul" consText)
           ,(pat ["mul","add"], printf "^(%s) * mul + add" consText)]]


{- | Format for FileIn.

> let rw nm rt nc sp pm = putStrLn (st_cons (nm, rt, nc, sp, pm))
> rw "SinOsc" (Left ar) (Right 1) 0 (["freq","phase"],Nothing)
> rw "Pan2" (Right [0]) (Right 2) 0 (["in","pos","level"],Nothing)
> rw "In" (Left ar) (Left (Left ())) 0 (["bus"],Nothing)
> rw "BrownNoise" (Left ar) (Right 1) 0 ([],Nothing)
> rw "MulAdd" (Right [0, 1]) (Right 1) 0 (["in","mul","add"],Nothing)
-}
st_cons :: St_Icm -> String
st_cons icm =
  let fmt (pat, def) = printf "%s\n\t%s!" pat def
      bdy = map fmt (st_instance_creation_methods icm)
      (ugen_nm, _, _, _, _) = icm
  in unlines
     [printf "!%s class methodsFor: 'instance creation'!" (Rename.sc3_ugen_rename ugen_nm)
     ,unlines (bdy ++ ["!"])]

st_gen :: String -> Either Rate [Int] -> String -> Bindings.Num_Chan -> Int -> Bindings.Param_Nm -> String
st_gen ugen_nm ugen_rt ugen_dsc num_chan special param_nm =
  unlines [st_class ugen_nm ugen_dsc param_nm
          ,st_cons (ugen_nm, ugen_rt, num_chan, special, param_nm)]

{- | Generate Smalltalk definitions for U

> pp = putStrLn . st_gen_u . Db.u_lookup_cs_err
> mapM_ pp ["LFGauss","EnvGen","LinExp","FM7","In"]
-}
st_gen_u :: Db.U -> String
st_gen_u u =
    let ugen_nm = Db.ugen_name u
        ugen_rt = maybe (Left (Db.ugen_default_rate u)) Right (Db.ugen_filter u)
        param_nm = Bindings.u_param_nm u
        num_chan = Bindings.u_num_chan u
        ugen_dsc = Db.ugen_summary u
    in st_gen ugen_nm ugen_rt ugen_dsc num_chan 0 param_nm

st_gen_binop :: [(String,Int)] -> String
st_gen_binop =
  let f (sym,ix) = printf "  %s b ^ BinaryOpUgen specialIndex: %d a: self b: b!" sym ix
  in unlines_with_point . (:) "!AbstractUgen methodsFor: 'arithmetic'!" . map f

st_gen_uop :: [(String,Int)] -> String
st_gen_uop =
  let f (sym,ix) = printf "  %s ^ UnaryOpUgen specialIndex: %d a: self!" sym ix
  in unlines_with_point . (:) "!AbstractUgen methodsFor: 'arithmetic'!" . map f

st_sc3_gen_bindings :: [(String, Int)] -> [(String, Int)] -> [String] -> [String]
st_sc3_gen_bindings uop binop ugen =
  let f = st_gen_u . Db.u_lookup_cs_err
  in [concatMap f ugen
     ,st_gen_binop binop
     ,st_gen_uop uop]

st_sc3_gen_bindings_wr :: FilePath -> [(String, Int)] -> [(String, Int)] -> [String] -> IO ()
st_sc3_gen_bindings_wr fn uop binop = writeFile fn . unlines . st_sc3_gen_bindings uop binop

{- | Generate list of required arguments to Ugen.

> map st_gen_required_arg Db.ugen_db
-}
st_gen_required_arg :: Db.U -> [String]
st_gen_required_arg u =
  let (param_nm,mce_nm) = Bindings.u_param_nm u
      num_chan = Bindings.u_num_chan u
      mcons e l = maybe l (:l) e
  in mcons (Bindings.num_chan_pseudo_input "numChan" num_chan) (Bindings.all_param_ty (param_nm,mce_nm))

-- * Filter methods

{- | Filter method for U where there is a single input parameter.
     This is for use with .stc notation so non-initial arguments have the name value:
-}
st_filter_method :: Db.U -> String
st_filter_method u =
  let delete_at k l = let (p,q) = splitAt k l in p ++ tail q
      nm = Db.ugen_name u
      mth = Rename.fromSc3Name nm
      ix = Db.u_filter_ix u
      inp = map Db.input_name (Db.ugen_inputs u)
      self = inp !! ix
      jn = (\i j -> concat [i,": ",j])
      arg = if length inp == 1
            then [mth]
            else zipWith jn (mth : replicate (length inp - 1) "value") (delete_at ix inp)
      cons = zipWith jn inp (map (\x -> if x == self then "self" else x) inp)
  in printf "  %s\n    ^%s %s\n!" (unwords arg) nm (unwords cons)

st_filter_methods :: [Db.U] -> String
st_filter_methods u =
  let forClass x =
        [printf "!%s methodsFor: 'filtering'!" x
        ,unlines (map st_filter_method u)
        ,"!"]
  in unlines (forClass "AbstractUgen" ++ forClass "Array")

-- * First input message

{- | Method for U where the first input is selected as the input parameter.
     This is for use with .stc notation so non-initial arguments have the name value:
-}
st_first_input_method :: Db.U -> String
st_first_input_method u =
  let nm = Db.ugen_name u
      mth = Rename.fromSc3Name nm
      inp = map Db.input_name (Db.ugen_inputs u)
      self = head inp
      jn = (\i j -> concat [i,": ",j])
      arg = if length inp == 1
            then [mth]
            else zipWith jn (mth : replicate (length inp - 1) "value") (tail inp)
      cons = zipWith jn inp (map (\x -> if x == self then "self" else x) inp)
  in printf "  %s\n    ^%s %s\n  !" (unwords arg) nm (unwords cons)

st_first_input_methods :: [Db.U] -> String
st_first_input_methods u =
  unlines ["!AbstractUgen methodsFor: 'filtering'!"
          ,unlines (map st_first_input_method u)
          ,"!"]
