{- | Blockly Ugen Bindings

There are three files:

Js - code generator, generates output Js code for block
Json - block definition
Xml - toolbox entry

-}
module Sound.Sc3.Ugen.Db.Bindings.Blockly where

import Data.Char {- base -}
import Data.List {- base -}
import Data.Maybe {- base -}
import Text.Printf {- base -}

import qualified Sound.Sc3.Ugen.Db as Db {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Bindings.Core as Bindings {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Pp as Pp {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Pseudo as Db {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Record as Db {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Rename as Rename {- hsc3-db -}

-- * Dict

type Dict_Elem = (String, Int, [String])

type Blk_Dict = [(String, Int, [Dict_Elem])]

{- | Colour sequence.

Colours are Hue values.

Primary colours and secondary colours:
0 = red, 60 = yellow, 120 = green, 180 = cyan, 240 = blue and 300 = magenta.
Tertiary colours:
30 = orange, 90 = chartreuse, 150 = spring, azure = 210, violet = 270, rose = 330.

Azure, Spring green, Orange, Magenta, Yellow
-}
clr_seq :: [Int]
clr_seq = [210, 150, 30, 300, 60]

clr_at :: Int -> Int
clr_at n = clr_seq !! (n - 1)

{- | Categorised Ugen list.
This list includes pseudo-Ugens, i.e. LinLin, TChoose, SinOscBank &etc.
The names are in normalised form, i.e. LfSaw not LFSaw.

-}
blk_dict :: Blk_Dict
blk_dict =
  -- 1
  [("Generator"
   ,1
    -- Osc
   ,[("Osc", 1, words "Blip Formant Impulse PmOsc Pulse Saw SinOsc SinOscBank SinOscFb SyncSaw VarSaw")
    ,("LfOsc", 1, words "LfCub LfGauss LfPar LfPulse LfSaw LfTri")
    ,("Model", 1, words "Bezier DwgPluckedStiff Dx7 Pluck Vosim")
   -- Noise
    ,("Noise", 1, words "BrownNoise Dust Dust2 DustRange GrayNoise PinkNoise WhiteNoise")
    ,("LfNoise", 1, words "LfNoise0 LfNoise1 LfNoise2 LfdNoise1 LfdNoise3 LfClipNoise")
    ,("Chaotic", 1, words "Crackle HenonL HenonC FbSineL FbSineC LorenzL QuadL QuadC")
    ,("Stochastic", 1, words "Gendy1")])
    -- 2
  ,("Processor"
   ,2
    -- Frequency
   ,[("Filter", 2, words "AllpassC AllpassL Bpf Brf Hpf Lag Lag2 Lag3 LagUd Lag3Ud Lpf OnePole")
    ,("Reson", 2, words "Formlet MoogFf MoogLadder ObdxFilter Resonz Ringz RingzBank Rhpf Rlpf")
    ,("BEQ", 2, words "BBandPass BBandStop BLowPass")
    ,("SVF", 2, words "SvfBp SvfHp SvfLp")
    ,("Fixed", 2, words "Bpz2 Hpz1 Lpz1")
    ,("Pitch", 2, words "FreqShift Pitch PitchShift")
    -- Time
    ,("Delay", 2, words "CombC CombL CombN DelayC DelayL DelayN")
    ,("Reverb", 2, words "FreeVerb FreeVerb2 GreyholeRaw GVerb")
    -- Level
    ,("Distortion", 2, words "Clip Fold MantissaMask Wrap")
    ,("Dynamics", 2, words "AmpComp Amplitude Limiter Normalizer")
    ,("Range", 2, words "LinExp LinLin")
      -- Util
    ,("Analysis", 2, words "LeakDc Pitch Slew Slope ZeroCrossing")
    ,("Branch", 2, words "Select Select2 SelectX")
    ,("Convert", 2, words "K2A")])
    -- 3
  ,("Trigger"
   ,3
   ,[("Trig", 3, words "Latch Phasor PulseCount PulseDivider RunningMax Sweep Stepper ToggleFf Trig")
    ,("Env", 3, words "  TLine TxLine")
    ,("Rand", 3, words " TChoose TExpRand TiRand TRand TScramble TxRand")
    ,("Grain", 3, words "GrainFm GrainSin TGrains")])
  ,("Demand"
   ,3
   ,[("Dmd", 3, words "DmdFor DmdOn TDmdFor")
    ,("Gen", 3, words "Seq Ser Shuf Choose")])
  ,("Fft"
   ,3
   ,[("Fft", 3, words "Fft Ifft")
    ,("Pv", 3, words "PvRandComb")])
  -- 4
  ,("Env"
   ,4
   ,[("Gate", 4, words "Adsr Asr Linen")
    ,("Gen", 4, words "LinSeg Ln XLn")
    ,("Flt", 4, words "Decay Decay2")])
  ,("Pan"
   ,4
   ,[("Mono", 4, words "LinPan2 Pan2 PanAz")
    ,("Stereo", 4, words "Balance2 LinXFade2 Rotate2 XFade2")
    ,("Array", 4, words " Splay Splay2")])
   -- 5
  ,("Random"
   ,5
   ,[("Gen", 5, words "ExpRand IRand LinRand NRand Rand")
    ,("Flt", 5, words "Hasher")])
  ,("Buffer"
   ,5
   ,[("Init", 5, words "BufAlloc BufFrames")
    ,("Math", 5, words "DegreeToKey")
    ,("Sf", 5, words "SfAcquire SfDur SfFrames SfPlay SfRateScale SfRead SfSampleRate")
    ,("RdWr", 5, words "BufRd  BufRec Freezer Index Osc ShufflerB")
    ,("Buf", 5, words "BufDur BufRateScale BufFrames")])
  ,("Io"
   ,5
   ,[("Ctl", 5, words "Cc KeyState MouseButton MouseX MouseY Sw")
    ,("Kbd", 5, words "KeyDown KeyPitch KeyVelocity KeyTimbre")
    ,("Snd", 5, words "AudioIn InFb LocalIn LocalOut Out ReplaceOut SampleRate")])
  ]

blk_dict_elem :: [Dict_Elem]
blk_dict_elem = concatMap (\(_, _, e) -> e) blk_dict

{- | The Ugen is renamed if required, i.e. this works for both normalised at Sc3 forms.

> mapMaybe blk_dict_lookup ["LFSaw", "LfSaw"]
-}
blk_dict_lookup :: String -> Maybe Dict_Elem
blk_dict_lookup nm = let f (_,_,l) = Rename.sc3_ugen_rename nm `elem` l in find f blk_dict_elem

blk_dict_lookup_err :: String -> Dict_Elem
blk_dict_lookup_err nm = fromMaybe (error ("blk_dict_lookup: " ++ nm)) (blk_dict_lookup nm)

blk_colour :: String -> Int
blk_colour nm = let (_,clr,_) = blk_dict_lookup_err nm in clr_seq !! (clr - 1)

-- * Js

{- | Js, code generator binding.
This is where the Ugen renaming is done.

> putStrLn $ blk_gen "SinOsc" ["freq", "phase"] True
> putStrLn $ blk_gen "LFSaw" ["freq", "iphase"] True
> putStrLn $ blk_gen "WhiteNoise" [] True
-}
blk_gen :: String -> [String] -> Bool -> String
blk_gen ugen_nm param_nm has_outputs =
  let in_single_quotes x = "'" ++ x ++ "'"
      input_names = intercalate ", " (map (in_single_quotes . map toUpper) param_nm)
  in printf
     "Blockly.JavaScript['sc3_%s'] = function(block) {\n  return blk_ugen_codegen(blk, block, '%s', [%s], %s);\n};"
     ugen_nm (Rename.sc3_ugen_rename ugen_nm) input_names (map toLower (show has_outputs))

-- * Json

-- | For block display, prefer concise texts and allow unicode symbols.  Case insensitive.
param_name_to_message :: String -> String
param_name_to_message nm =
  case map toLower nm of
    "freq" -> "ν"
    "fundfreq" -> "ν₀"
    "formantfreq" -> "ν𝑓"
    "formfreq" -> "ν𝑓"
    "bwfreq" -> "ν↕"
    "syncfreq" -> "ν↲"
    "sawfreq" -> "ν◿"
    "minfreq" -> "⌈ν"
    "maxfreq" -> "⌊ν"
    "initfreq" -> "𝑖ν"
    "execfreq" -> "⤴ν"
    "carfreq" -> "ν𝑐"
    "modfreq" -> "ν𝑚"
    "phase" -> "ϕ"
    "iphase" -> "ϕ"
    "modphase" -> "ϕ𝑚"
    "width" -> "↔"
    "bw" -> "↕"
    "mul" -> "×"
    "amp" -> "×"
    "level" -> "×"
    "add" -> "+"
    "decaytime" -> "↘"
    "decay" -> "↘"
    "attacktime" -> "↗"
    "releasetime" -> "⇲"
    "sustainlevel" -> "⎺"
    "in" -> "∙"
    "in1" -> "∙₁"
    "in2" -> "∙₂"
    "ina" -> "∙₁"
    "inb" -> "∙₂"
    "input" -> "∙"
    "inarray" -> "⟦∙⟧"
    "channelarray" -> "⟦∙⟧"
    "channelsarray" -> "⟦∙⟧"
    "demandugens" -> "∙"
    "lo" -> "⌊"
    "srclo" -> "⌊"
    "minval" -> "⌊"
    "dstlo" -> "⌋"
    "hi" -> "⌉"
    "dsthi" -> "⌉"
    "maxval" -> "⌈"
    "srchi" -> "⌈"
    "minmax" -> "⌊⌈"
    "trig" -> "⤴"
    "trigger" -> "⤴"
    "time" -> "⏲"
    "dur" -> "⏲"
    "duration" -> "⏲"
    "lagtime" -> "⏲"
    "lagtimeu" -> "⏲↑"
    "lagtimed" -> "⏲↓"
    "array" -> "⟦⟧"
    "coord" -> "⟦⟧"
    "density" -> "▩"
    "delaytime" -> "⎵"
    "maxdelaytime" -> "⌈⎵"
    "numchannels" -> "#"
    "numberofchannels" -> "#"
    "inputs" -> "⟦∙⟧"
    "default" -> "⎕"
    "reset" -> "↻"
    "resetPos" -> "↻"
    "resetval" -> "↻"
    "loop" -> "↻"
    "numharm" -> "#"
    "ncycles" -> "#"
    "start" -> "⟜"
    "end" -> "⊸"
    "doneaction" -> "⌫"
    "warp" -> "𝑓"
    "curves" -> "⟦𝑓⟧"
    "interp" -> "𝑓"
    "interpolation" -> "𝑓"
    "lag" -> "⎎"
    "left" -> "𝐿"
    "right" -> "𝑅"
    "pos" -> "⌖"
    "pan" -> "⌖"
    "room" -> "▣"
    "roomsize" -> "▣"
    "maxroomsize" -> "⌈▣"
    "revtime" -> "⏲"
    "spread" -> "◠"
    "drylevel" -> "∙×"
    "earlyreflevel" -> "◿×"
    "taillevel" -> "◺×"
    "damping" -> "⊗"
    "damp" -> "⊗"
    "inputbw" -> "∙↕"
    "rq" -> "¹⁄𝑄"
    "bwr" -> "¹⁄𝑄"
    "div" -> "÷"
    "repeats" -> "#"
    "chaosparam" -> "𝛼"
    "bufnum" -> "⛁"
    "buffer" -> "⛁"
    "sfbufferarray" -> "⟦⛁⟧"
    "envbufnum" -> "⏢⛁"
    "gate" -> "⎇"
    "which" -> "☞"
    "coef" -> "⊗"
    "octave" -> "8𝒗"
    "index" -> "☞"
    "channelindices" -> "⟦☞⟧"
    "pmindex" -> "☞𝑚"
    "iftrue" -> "⊤"
    "iffalse" -> "⊥"
    "feedback" -> "↻"
    "mix" -> "⊕"
    "keycode" -> "#"
    "iotMin" -> "⌈"
    "iotMax" -> "⌊"
    "min" -> "⌈"
    "max" -> "⌊"
    "numFrames" -> "#⟦⟧"
    "windowSize" -> "#⌓"
    "pitchRatio" -> "𝅘𝅥𝅮÷"
    "pitchDispersion" -> "𝅘𝅥𝅮⢅"
    "timeDispersion" -> "⏲⢅"
    _ -> nm

{- | For block display, prefer concise texts and allow unicode symbols.
Renames input so will work for either form.

> map ugen_name_to_message ["LFSaw", "LfSaw"]
-}
ugen_name_to_message :: String -> String
ugen_name_to_message nm =
  case Rename.sc3_ugen_rename nm of
    "SinOsc" -> "∿"
    "SinOscFb" -> "∿↻"
    "SinOscBank" -> "∿┋"
    "PmOsc" -> "ϕ∿"
    "Pulse" -> "⎍"
    "LfPulse" -> "␊⎍"
    "Impulse" -> "⊥"
    "Decay" -> "↘"
    "Decay2" -> "↘₂"
    "Dust" -> "⢅"
    "Dust2" -> "⢅₂"
    "DustRange" -> "⢅↔"
    "CombC" -> "ᚊ"
    "CombL" -> "ᚊ𝐿"
    "DelayC" -> "⎵"
    "DelayL" -> "⎵𝐿"
    "DelayN" -> "⎵𝑁"
    "Rand" -> "⚁"
    "IRand" -> "𝐼⚁"
    "NRand" -> "𝑁⚁"
    "ExpRand" -> "𝑋⚁"
    "LinRand" -> "𝐿⚁"
    "TExpRand" -> "⤴𝑋⚁"
    "TRand" -> "⤴⚁"
    "TiRand" -> "⤴𝐼⚁"
    "Trig" -> "⤴"
    "PulseCount" -> "⤴#"
    "PulseDivider" -> "⤴÷"
    "Ln" -> "―"
    "XLn" -> "𝑋―"
    "TLine" -> "⤴―"
    "TxLine" -> "⤴𝑋―"
    "MouseButton" -> "☟↕"
    "MouseX" -> "☟𝑥"
    "MouseY" -> "☟𝑦"
    "KeyState" -> "⌨⎇"
    "LfTri" -> "△"
    "Saw" -> "◿"
    "LfSaw" -> "␊◿"
    "SyncSaw" -> "↲◿"
    "VarSaw" -> "𝑉◿"
    "Splay" -> "◠"
    "Splay2" -> "◠₂"
    "Clip" -> "⊏"
    "Lag" -> "⎎"
    "Lag2" -> "⎎₂"
    "Lag3" -> "⎎₃"
    "LagUd" -> "⎎↕"
    "Lag3Ud" -> "⎎₃↕"
    "Dc" -> "⎓"
    "Pan2" -> "⌖₂"
    "LinPan2" -> "𝐿⌖₂"
    "Asr" -> "⏢"
    "WhiteNoise" -> "⍰𝒘"
    "PinkNoise" -> "⍰𝒑"
    "BrownNoise" -> "⍰𝒃"
    "GrayNoise" -> "⍰𝒈"
    "LfNoise0" -> "␊⍰₀"
    "LfNoise1" -> "␊⍰₁"
    "LfNoise2" -> "␊⍰₂"
    "LfdNoise1" -> "␊D⍰₁"
    "LfdNoise3" -> "␊D⍰₃"
    "Resonz" -> "⋏"
    "Ringz" -> "⋏ₜ"
    "RingzBank" -> "⋏ₜ┋"
    "Lpf" -> "⬔"
    "Hpf" -> "◩"
    "Rlpf" -> "⋏⬔"
    "Rhpf" -> "⋏◩"
    "AllpassC" -> "⬜"
    "AllpassL" -> "⬜𝐿"
    "Latch" -> "⍀"
    "Lpz1" -> "⬔𝑧₁"
    "Hpz1" -> "◩𝑧₁"
    "Bpz2" -> "◫𝑧₂"
    "Bpf" -> "◫"
    "LeakDc" -> "⭭⎓"
    "LinExp" -> "/ノ"
    "LinLin" -> "//"
    "Slope" -> "𝑓′"
    "GVerb" -> "⧈𝒈"
    "FreeVerb" -> "⧈𝑓"
    "FreeVerb2" -> "⧈𝑓₂"
    "TChoose" -> "⤴⟦⟧⚁"
    "BLowPass" -> "B⬔"
    "BBandPass" -> "B◫"
    "ZeroCrossing" -> "⌀"
    "PvRandComb" -> "㎴⚁ᚊ"
    "PitchShift" -> "𝅘𝅥𝅮↕"
    "AudioIn" -> "∙"
    _ -> nm

{- | Json, message parameters.

> blk_dfn_param (words "ringTime")
> blk_dfn_param (words "freq phase × +")
-}
blk_dfn_param :: [String] -> String
blk_dfn_param =
  let f k nm = printf "%s %%%d" (param_name_to_message nm) k
  in unwords . zipWith f [2::Int ..]

single_quote_to_double_quote :: Char -> Char
single_quote_to_double_quote c = if c == '\'' then '"' else c

{- | Json, input value.

> blk_dfn_input False "freq"
> blk_dfn_input True "freq"
-}
blk_dfn_input :: Bool -> String -> String
blk_dfn_input withCheck name =
  let checkStr = if withCheck then ", 'check': ['Number', 'Array', 'UGen', 'Boolean', 'String']" else ""
  in printf "            {'type': 'input_value', 'name': '%s'%s}" (map toUpper name) checkStr

-- | × +
with_muladd :: Bool -> [String] -> Bool -> [String]
with_muladd sym param_nm has_outputs =
  param_nm ++ if has_outputs then (if sym then ["×", "+"] else ["mul", "add"]) else []

{- | Json, block definition.
This has Ugen names in Sc3 form, i.e. LFSaw &etc.
The renaming is done in the code-generator code-generator.

> putStrLn $ blk_dfn "SinOsc" (words "freq phase") "Interpolating sine wavetable oscillator" "SinOsc" True (blk_colour "SinOsc")
-}
blk_dfn :: String -> [String] -> String -> String -> Bool -> Int -> String
blk_dfn ugen_nm param_nm ugen_dsc ugen_class has_outputs clr =
  (map single_quote_to_double_quote . unlines)
  [       "    {"
  ,printf "        'type': 'sc3_%s'," ugen_nm
  ,printf "        'message0': '%s %%1 %s'," (ugen_name_to_message ugen_nm) (blk_dfn_param (with_muladd True param_nm has_outputs))
  ,       "        'args0': ["
  ,       "            {'type': 'input_dummy'},"
  ,intercalate ",\n" (map (blk_dfn_input True) (with_muladd False param_nm has_outputs))
  ,       "        ],"
  ,       "        'output': 'UGen',"
  ,printf "        'colour': %d," clr
  ,printf "        'tooltip': '%s'," ugen_dsc
  ,printf "        'helpUrl': 'https://doc.sccode.org/Classes/%s.html'"ugen_class
  ,       "    }"
  ]

-- * Xml

{- | Xml, input shadow (default value).

> blk_shadow ("freq",440)
-}
blk_shadow :: (String, Double) -> String
blk_shadow (nm, df) =
  printf
  "      <value name='%s'><shadow type='math_number'><field name='NUM'>%f</field></shadow></value>"
  (map toUpper nm)
  df

{- | Xml, toolbox Ugen block definition.

> putStrLn $ blk_tool "SinOsc" (zip (words "freq phase") [440, 0]) True
-}
blk_tool :: String -> [(String, Double)] -> Bool -> String
blk_tool ugen_nm param_def has_outputs =
  unlines
  [printf "    <block type='sc3_%s' inline='true'>" ugen_nm
  ,intercalate "\n" (map blk_shadow (param_def ++ if has_outputs then [("mul", 1),("add", 0)] else []))
  ,       "    </block>"]

blk_case_rule :: String -> String
blk_case_rule = id -- map toLower

-- * Ugen Db

u_get_param :: Db.U -> [(String,Double)]
u_get_param u =
  let param_nm = Bindings.all_param_ty (Bindings.u_param_nm u)
      param_def = Bindings.all_param_ty (Bindings.u_param_def u)
      param = zip param_nm param_def
  in case Bindings.num_chan_pseudo_input "numChannels" (Bindings.u_num_chan u) of
       Just nm -> (nm, 1) : param
       Nothing -> param

-- > putStrLn $ u_blk_gen (Db.u_lookup_cs_err "TGrains")
-- > putStrLn $ u_blk_gen (Db.u_lookup_cs_err "LFSaw")
u_blk_gen :: Db.U -> String
u_blk_gen u =
  let ugen_nm = Db.ugen_name u
      (param_nm, _param_def) = unzip (u_get_param u)
      num_chan = Bindings.u_num_chan u
  in blk_gen (blk_case_rule ugen_nm) param_nm (not (Bindings.num_chan_is_sink num_chan))

u_blk_dfn :: Db.U -> String
u_blk_dfn u =
  let ugen_nm = Db.ugen_name u
      (param_nm,_) = unzip (u_get_param u)
      num_chan = Bindings.u_num_chan u
      ugen_dsc = concat [Pp.u_block_help_pp u, ": ", Db.ugen_summary u]
  in blk_dfn (blk_case_rule ugen_nm) param_nm ugen_dsc ugen_nm (not (Bindings.num_chan_is_sink num_chan)) (blk_colour ugen_nm)

u_blk_tool :: Db.U -> String
u_blk_tool u =
  let ugen_nm = Db.ugen_name u
      param = u_get_param u
      num_chan = Bindings.u_num_chan u
  in blk_tool (blk_case_rule ugen_nm) param (not (Bindings.num_chan_is_sink num_chan))

-- * Pseudo Db

p_blk_gen :: Db.Pseudo_Ugen -> String
p_blk_gen (nm, arg, out, _dsc, _def) = blk_gen nm arg out

p_blk_dfn :: Db.Pseudo_Ugen -> String
p_blk_dfn (nm, arg, out, dsc, _def) = blk_dfn nm arg dsc nm out (blk_colour nm)

p_blk_tool :: Db.Pseudo_Ugen -> String
p_blk_tool (nm, arg, out, _dsc, def) = blk_tool nm (zip arg def) out

{-
> putStrLn $ unlines $ map p_blk_gen Db.pseudo_ugen_db
> putStrLn $ unlines $ map p_blk_dfn Db.pseudo_ugen_db
> putStrLn $ unlines $ map p_blk_tool Db.pseudo_ugen_db
-}

-- * Categorised

in_cat :: String -> Int -> String -> String
in_cat nm clr bdy = printf "  <category name='%s' colour='%d'>\n%s  </category>" nm (clr_at clr) bdy

blk_tool_set :: Dict_Elem -> String
blk_tool_set (cat,clr,lst) =
  let u_seq = filter (\u -> Db.ugen_name u `elem` lst) Db.ugen_db
      p_seq = filter (\p -> Db.pseudo_ugen_name p `elem` lst) Db.pseudo_ugen_db
  in in_cat cat clr (concatMap u_blk_tool u_seq ++ concatMap p_blk_tool p_seq)

blk_tool_cat :: (String, Int, [Dict_Elem]) -> String
blk_tool_cat (cat, clr, elm) = in_cat cat clr (unlines (map blk_tool_set elm))
