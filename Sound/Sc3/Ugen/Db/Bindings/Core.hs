-- | Shared types and functions for bindings.
module Sound.Sc3.Ugen.Db.Bindings.Core where

import Data.Maybe {- base -}
import Data.Either {- base -}

import qualified Sound.Sc3.Ugen.Db.Record as DB {- hsc3-db -}

-- | Parameters partitioned as (ordinary,mce).
type Param_Ty t = ([t],Maybe [t])

-- | Parameter names
type Param_Nm = Param_Ty String

-- | Parameter default values
type Param_Def = Param_Ty Double

-- | Number of parameters, ie. sum of ordinary and mce parameters.
param_ty_size :: Param_Ty t -> Int
param_ty_size (p,m) = length p + maybe 0 length m

-- | Singular MCE input parameter, else 'error'
param_ty_mce_input :: Param_Ty t -> t
param_ty_mce_input pn =
  case pn of
    (_,Just [m]) -> m
    _ -> error "st_param_ty_mce_input?"

-- > all_param_ty (["freq","phase"],Nothing) == ["freq","phase"]
-- > all_param_ty (["bus"],Just ["channelsArray"]) == ["bus","channelsArray"]
all_param_ty :: Param_Ty t -> [t]
all_param_ty (param,mce) = param ++ fromMaybe [] mce

u_param_nm :: DB.U -> Param_Nm
u_param_nm u = bindings_mce u (map DB.input_name (DB.ugen_inputs u))

u_param_def :: DB.U -> Param_Def
u_param_def u = bindings_mce u (map DB.input_default (DB.ugen_inputs u))

-- * Mce

-- | 'splitAt' with index from right.
--
-- > split_at_right 5 "x.text" == ("x",".text")
split_at_right :: Int -> [t] -> ([t],[t])
split_at_right n l = splitAt (length l - n) l

-- | Split inputs into (ordinary,mce).
--
-- > let u = DB.u_lookup_cs_err "XOut"
-- > bindings_mce u (u_input_names_proc_rename Nothing u) == (["bus","xfade"],Just ["input"])
bindings_mce :: DB.U -> [t] -> ([t],Maybe [t])
bindings_mce u i =
  let n = DB.ugen_std_mce u
  in if n > 0
     then let (lhs,rhs) = split_at_right n i
          in (lhs,Just rhs)
     else (i,Nothing)

-- * Num Chan

{- | Cases are:
Right Int = fixed number of channels
Left (Left ()) = number of channels pseudo-input
Left (Right (String,Int)) = num_chan is mceInput size + offset
-}
type Num_Chan = Either (Either () (String,Int)) Int

-- | Is fixed number of channels.
num_chan_is_fixed :: Num_Chan -> Bool
num_chan_is_fixed = isRight

-- | Is number of channels fixed at zero
num_chan_is_sink :: Num_Chan -> Bool
num_chan_is_sink nc =
  case nc of
    Right 0 -> True
    _ -> False

-- | Either 'show' of the fixed number of channels or /nil/.
num_chan_fixed_str :: String -> Num_Chan -> String
num_chan_fixed_str nil nc =
  case nc of
    Right k -> show k
    _ -> nil

-- | If Num_Chan indicates there is a numChans input return /nm/ (it's name).
num_chan_pseudo_input :: String -> Num_Chan -> Maybe String
num_chan_pseudo_input nm nc =
  case nc of
    Left (Left ()) -> Just nm
    _ -> Nothing

num_chan_has_pseudo_input :: Num_Chan -> Bool
num_chan_has_pseudo_input = isJust . num_chan_pseudo_input ""

  -- | If Num_Chan indicates there is an mce input that determines the output channel count, derive it.
num_chan_mce_input :: ((String,Int) -> String) -> Num_Chan -> Maybe String
num_chan_mce_input mk nc =
  case nc of
    Left (Right x) -> Just (mk x)
    _ -> Nothing

u_num_chan :: DB.U -> Num_Chan
u_num_chan u =
  if DB.ugen_nc_input u
  then Left (Left ())
  else case DB.ugen_nc_mce u of
         Just k -> Left (Right (param_ty_mce_input (u_param_nm u),k))
         Nothing -> Right (fromMaybe (error (show ("NC",u))) (DB.ugen_outputs u))
