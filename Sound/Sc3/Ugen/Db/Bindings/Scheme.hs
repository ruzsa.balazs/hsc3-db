-- | Generate (approximate) Scheme Ugen Bindings
module Sound.Sc3.Ugen.Db.Bindings.Scheme where

import Data.Char {- base -}
import Data.Maybe {- base -}
import Text.Printf {- base -}

import qualified Sound.Sc3.Common.Base as Base {- hsc3 -}
import qualified Sound.Sc3.Common.Math.Operator as Operator {- hsc3 -}
import qualified Sound.Sc3.Common.Rate as Rate {- hsc3 -}
import qualified Sound.Sc3.Ugen.Name as Name {- hsc3 -}

import qualified Sound.Sc3.Ugen.Db as DB {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Bindings.Core as Bindings {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Record as Record {- hsc3-db -}
import qualified Sound.Sc3.Ugen.Db.Rename as Rename {- hsc3-db -}

{- | Names for rates.

> map scheme_rate_id Rate.all_rates == ["scalar-rate","control-rate","audio-rate","demand-rate"]
-}
scheme_rate_id :: Rate.Rate -> String
scheme_rate_id rt =
  fromMaybe (error "scheme_rate_id?")
  (lookup (fromEnum rt) (zip [0..] (words "scalar-rate control-rate audio-rate demand-rate")))

scheme_list_pp :: [String] -> String
scheme_list_pp x = "(list " ++ unwords x ++ ")"

scheme_rate :: Record.U -> (Maybe String,String)
scheme_rate u =
    case Record.ugen_filter u of
      Just ix -> (Nothing,scheme_list_pp (map show ix))
      Nothing -> case Record.ugen_fixed_rate u of
                   Just fx -> (Nothing,scheme_rate_id fx)
                   Nothing -> (Just "rt","rt")

scheme_nc :: Record.U -> String -> (Maybe String,String)
scheme_nc u mc =
    case Record.u_fixed_outputs u of
      Just d -> (Nothing,show d)
      Nothing ->
          if Record.ugen_nc_input u
          then (Just "nc","nc")
          else if Record.ugen_std_mce u > 0
               then (Nothing,printf "(length (mceChannels %s))" mc)
               else error "NC?"

scheme_u_gen_binding_maybe_operator :: (String -> String) -> (String,Maybe Int) -> Record.U -> String
scheme_u_gen_binding_maybe_operator rw (nm',sp) u =
  let nm = Record.ugen_name u
      i = Rename.u_renamed_inputs u
      (i',mc) = Bindings.bindings_mce u i
      mc' = maybe "nil" unwords mc
      i_k = if null i' then "nil" else "(list " ++ unwords i' ++ ")"
      (rt_var,rt_k) = scheme_rate u
      (nc_var,nc_k) = scheme_nc u mc'
      sp' = maybe "nil" show sp
      param = mcons nc_var (mcons rt_var i)
      scheme_nm = rw nm'
      template_f = concat ["(define %s\n"
                          ,"  (lambda (%s)\n"
                          ,"    (construct-ugen \"%s\" %s %s %s %s %s)))\n"]
      template_v = "(define %s (construct-ugen \"%s\" %s %s %s %s %s))\n"
  in if null param
     then printf template_v scheme_nm nm rt_k i_k mc' nc_k sp'
     else printf template_f scheme_nm (unwords param) nm rt_k i_k mc' nc_k sp'

-- > map scheme_name_rw ["SinOsc", "LFSaw"] == ["sin-osc", "lf-saw"]
scheme_name_rw :: String -> String
scheme_name_rw = Rename.scheme_rename . Name.sc3_name_to_lisp_name

{- | mk-ugen name rate|[ix] inputs mce-input|nil nc special|nil

> let u = words "SinOsc Resonz Demand Drand Dwhite BinaryOpUGen SampleRate Abs Rand Fdiv"
> let u = DB.complete_names DB.ugen_db
> mapM_ (putStrLn . scheme_mk_ugen id) u
-}
scheme_mk_ugen :: (String -> String) -> String -> String
scheme_mk_ugen rw nm =
  let (nm',sp) = Operator.resolve_operator Base.Cs nm
      u = case DB.u_lookup Base.Ci nm' of
            Nothing -> error ("scheme_mk_ugen: unknown ugen: " ++ nm')
            Just r -> r
  in scheme_u_gen_binding_maybe_operator rw (nm,sp) u

scheme_u_gen_binding :: (String -> String) -> Record.U -> String
scheme_u_gen_binding rw u = scheme_u_gen_binding_maybe_operator rw (Record.ugen_name u,Nothing) u

scheme_lowercase_name :: String -> String
scheme_lowercase_name = Rename.scheme_rename . map toLower

{- | mk-ugen name rate|[ix] inputs mce-input|nil nc special|nil

The constUgens option will generate a value rather than a function for SampleRate &etc.

> let u = "SinOsc Resonz Demand Drand Dwhite BinaryOpUGen SampleRate Abs Rand WhiteNoise RCD LFSaw"
> mapM_ (putStrLn . scheme_mk_ugen_arf (False, Rename.sc3_ugen_rename)) (words u)
-}
scheme_mk_ugen_arf :: (Bool, String -> String) -> String -> String
scheme_mk_ugen_arf (constUgens, nameRewriter) nm =
  let (nm',sp) = Operator.resolve_operator Base.Cs nm
      u = case DB.u_lookup Base.Ci nm' of
            Nothing -> error ("scheme_mk_ugen_arf: unknown ugen: " ++ nm')
            Just r -> r
      i = Rename.u_renamed_inputs u
      (i',mc) = Bindings.bindings_mce u i
      mc' = maybe "nil" unwords mc
      i_k = if null i' then "nil" else "(list " ++ unwords i' ++ ")"
      (nc_var,nc_k) = scheme_nc u mc'
      sp' = maybe "nil" show sp
      param = mcons nc_var i
      (rt_var,rt_rhs) = scheme_rate u
      rt_k = maybe rt_rhs (const (scheme_rate_id (Record.ugen_default_rate u))) rt_var
      template_f = concat ["(define %s\n"
                          ,"  (lambda (%s)\n"
                          ,"    (construct-ugen \"%s\" %s %s %s %s %s)))\n"]
      template_v = "(define %s (construct-ugen \"%s\" %s %s %s %s %s))\n"
  in if constUgens && null param && not (Record.ugen_nondet u)
     then printf template_v (nameRewriter nm) nm rt_k i_k mc' nc_k sp'
     else printf template_f (nameRewriter nm) (unwords param) nm' rt_k i_k mc' nc_k sp'

{- | The generated bindings are for @SinOsc@ etc, alias these to @sin-osc@ etc.

> writeFile "/tmp/alias.scm" (unlines (map (scheme_mk_alias True) (DB.complete_names DB.ugen_db)))
-}
scheme_mk_alias :: Bool -> String -> String
scheme_mk_alias dir nm =
  let scheme_nm = Rename.scheme_rename (Name.sc3_name_to_lisp_name nm)
  in if dir then printf "(define %s %s)" nm scheme_nm else printf "(define %s %s)" scheme_nm nm

mcons :: Maybe a -> [a] -> [a]
mcons e = case e of {Nothing -> id; Just e' -> (e' :)}

-- > mapM_ putStrLn (operator_sym_def id)
-- > mapM_ putStrLn (operator_sym_def Name.sc3_name_to_lisp_name)
operator_sym_def :: (String -> String) -> [String]
operator_sym_def rw =
    let f (bin,sym_nm) =
            let sc3_nm = show bin
            in printf "(define %s %s)" sym_nm (rw sc3_nm)
    in map f Operator.binary_sym_tbl

-- > mapM_ putStrLn (scheme_rename_def Rename.scheme_rename)
scheme_rename_def :: (String -> String) -> [String]
scheme_rename_def rw =
    let f nm = printf "(define %s %s)" nm (rw nm)
    in map f Rename.scheme_names
