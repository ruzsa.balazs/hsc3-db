-- | Db entries for Pseudo-Ugens
module Sound.Sc3.Ugen.Db.Pseudo where

import Data.List {- base -}

-- | (Name, InputNames, HasOuputs?, Description, DefaultInputs)
type Pseudo_Ugen = (String, [String], Bool, String, [Double])

-- | Name of Pseudo_Ugen
pseudo_ugen_name :: Pseudo_Ugen -> String
pseudo_ugen_name (nm, _, _, _, _) = nm

-- | List of pseudo Ugens.
pseudo_ugen_db :: [Pseudo_Ugen]
pseudo_ugen_db =
  [("Adsr"
   ,words "gate attackTime decayTime sustainLevel releaseTime curves"
   ,True
   ,"Attack-decay-sustain-release envelope"
   ,[1, 0.01, 0.3, 0.5, 1, -4])
  ,("Asr"
   ,words "gate attackTime releaseTime curves"
   ,True
   ,"Attack-sustain-release envelope"
   ,[1, 0.01, 1, -4])
  ,("AudioIn"
   ,words "channelArray"
   ,True
   ,"Audio Input"
   ,[0])
  ,("BufAlloc"
   ,words "numChannels numFrames"
   ,True
   ,"Allocate a buffer local to the synth"
   ,[1, 9])
  ,("BufRec"
   ,words "bufnum reset inputArray"
   ,True
   ,"Record signal into a buffer"
   ,[0, 0, 0])
  ,("Cc"
   ,words "index"
   ,True
   ,"Read continuous controller input at index"
   ,[1])
  ,("Choose"
   ,words "repeats array"
   ,True
   ,"Random sequence generator with repeats count"
   ,[1, 0])
  ,("DmdFor"
   ,words "dur reset level"
   ,True
   ,"Demand results from demand rate Ugens at intervals"
   ,[0, 0, 0])
  ,("TDmdFor"
   ,words "dur reset level"
   ,True
   ,"Demand results as trigger from demand rate Ugens at intervals"
   ,[0, 0, 0])
  ,("DmdOn"
   ,words "trig reset demandUgens"
   ,True
   ,"Demand results from demand rate Ugens on trigger"
   ,[0, 0, 0])
  ,("InFb"
   ,words "numChannels bus"
   ,True
   ,"Read signal from a bus with a current or one cycle old timestamp"
   ,[1, 0])
  ,("KeyDown", words "voiceNumber", True, "Key down status of voice", [1])
  ,("KeyPitch", words "voiceNumber", True, "Key pitch status of voice", [1])
  ,("KeyVelocity", words "voiceNumber", True, "Key velocity status of voice", [1])
  ,("KeyTimbre", words "voiceNumber", True, "Key timbre status of voice", [1])
  ,("KeyPressure", words "voiceNumber", True, "Key pressure status of voice", [1])
  ,("LinLin"
   ,words "in srclo srchi dstlo dsthi"
   ,True
   ,"Linear range mapping"
   ,[0, -1, 1, 0, 1])
  ,("LinSeg"
   ,words "gate coord"
   ,True
   ,"Line segment envelope"
   ,[0, 0])
  ,("Ln"
   ,words "start end dur"
   ,True
   ,"Line generator"
   ,[0, 1, 1])
  ,("PmOsc"
   ,words "carfreq modfreq pmindex modphase"
   ,True
   ,"Phase modulation oscillator pair"
   ,[440, 1, 0, 0])
  ,("RingzBank"
   ,words "input freq amp time"
   ,True
   ,"Parallel bank of Ringz (Klank)"
   ,[1, 440, 0.1, 1])
  ,("Select2"
   ,words "predicate ifTrue ifFalse"
   ,True
   ,"Select signal branch based on predicate value"
   ,[1, 1, 0])
  ,("SelectX"
   ,words "which array"
   ,True
   ,"Mix one output from many sources"
   ,[0, 0])
  ,("Seq"
   ,words "repeats array"
   ,True
   ,"Sequence generator with repeats count"
   ,[1, 0])
  ,("Ser"
   ,words "length start step"
   ,True
   ,"Demand rate arithmetic series Ugen"
   ,[9, 0, 1])
  ,("SfAcquire", words "urlOrKey numberOfChannels channelIndices", True, "Acquire SoundFile buffers", [0, 0, 0])
  ,("SfDur", words "sfBufferArray", True, "SoundFile duration (seconds)", [0])
  ,("SfFrames", words "sfBufferArray", True, "SoundFile frame count", [0])
  ,("SfPlay", words "sfBufferArray rate trigger startPos loop", True, "Sound file player", [0, 1, 1, 0, 0])
  ,("SfRateScale", words "sfBufferArray", True, "SoundFile multiplier relating sound file and system sample rates", [0])
  ,("SfRead", words "sfBufferArray phase loop interpolation", True, "SoundFile reader", [0, 0, 0, 0])
  ,("SfSampleRate", words "sfBufferArray", True, "SoundFile sample rate (hz)", [0])
  ,("Shuf"
   ,words "repeats array"
   ,True
   ,"Random sequence generator with repeats count"
   ,[1, 0])
  ,("SinOscBank"
   ,words "freq amp phase"
   ,True
   ,"Parallel bank of SinOsc (Klang)"
   ,[440, 0.1, 0])
  ,("Splay"
   ,words "input spread level center levelComp"
   ,True
   ,"Pan an array of channels across the stereo field"
   ,[0, 1, 1, 0, 1])
  ,("Splay2"
   ,words "inArray"
   ,True
   ,"Pan an array of channels across the stereo field"
   ,[0])
  ,("Sw"
   ,words "index"
   ,True
   ,"Read switch controller input at index"
   ,[1])
  ,("TChoose"
   ,words "trig array"
   ,True
   ,"Randomly select one of several inputs"
   ,[1, 0])
  ,("TLine"
   ,words "start end dur trig"
   ,True
   ,"Triggered Line"
   ,[0, 1, 1, 1])
  ,("TxLine"
   ,words "start end dur trig"
   ,True
   ,"Triggered XLine"
   ,[0.001, 1, 1, 1])
  ,("Tr", words "in", True, "Trig with duration of one quantum (sample or control duration)", [0])
  ,("Tr1", words "in", True, "Trig1 with duration of one quantum (sample or control duration)", [0])
  ,("XLn"
   ,words "start end dur"
   ,True
   ,"Exponential line generator"
   ,[1, 2, 1])
  ]

-- | Psuedo Ugen lookup
pseudo_ugen_db_lookup :: String -> Maybe Pseudo_Ugen
pseudo_ugen_db_lookup nm = find ((== nm) . pseudo_ugen_name) pseudo_ugen_db

{- | Is nm a Pseudo Ugen?

> is_pseudo_ugen "SinOscBank" == True
-}
is_pseudo_ugen :: String -> Bool
is_pseudo_ugen nm = nm `elem` (map pseudo_ugen_name pseudo_ugen_db)
